import {Component, ViewEncapsulation, OnInit, ViewChild, TemplateRef} from '@angular/core';
import {ChartsService} from './charts.service';
import * as Highcharts from 'highcharts';
import {UxTable} from '@netcracker/ux-ng2/library/table';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less'],
    encapsulation: ViewEncapsulation.None
})

export class AppComponent implements OnInit {
    @ViewChild("uxTable")
    private uxTable: UxTable;
    @ViewChild("customCellContent")
    private customCellContent: TemplateRef<any>;
    highcharts = Highcharts;
    chartOptions: any;
    constructor(private chartS: ChartsService) {
    }
    ngOnInit() {
        this.chartS.data().subscribe((data) => {
            this.chartOptions = data;
            this._tableModel = {
            header: {
                rows: [
                    {
                        styleClass: "_header",
                        columns: [
                            {
                                value: "Name",
                                styleClass: "_header",
                            }
                        ]
                    }
                ]
            },
            body: {
                rows: [
                    {
                        columns: [
                            {
                                type: 'content',
                                value: this.customCellContent,
                                contentModel: {
                                    graphModel: this.chartOptions[0]
                                }
                            }
                        ]
                    },
                    {
                        columns: [
                            {
                                type: 'content',
                                value: this.customCellContent,
                                contentModel: {
                                    graphModel: this.chartOptions[1]
                                }
                            }
                        ]
                    }
                ]
            }
        }; } );
    }
    _tableModel: UxTable;
}
