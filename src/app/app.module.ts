import {BrowserModule, EVENT_MANAGER_PLUGINS} from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {UxButtonModule} from '@netcracker/ux-ng2/library/button';
import {UxHammerPluginPatchA6} from '@netcracker/ux-ng2/library/patches';
import { HighchartsChartComponent } from 'highcharts-angular';
import {HttpClientModule} from '@angular/common/http';
import {UxTableModule} from '@netcracker/ux-ng2/library/table';

@NgModule({
  declarations: [
    AppComponent,
      HighchartsChartComponent
  ],
  imports: [
    BrowserModule,
    UxButtonModule,
      HttpClientModule,
      UxTableModule,
  ],
  providers: [
      // fix hammerjs bugs http://wsm-0911:3003/patches/hammer-plugin
      {
          provide: EVENT_MANAGER_PLUGINS,
          useClass: UxHammerPluginPatchA6,
          multi: true
      }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
